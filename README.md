# Prepare environment

## Run database migrations
```
docker-compose run --rm web python /code/manage.py migrate
```

## Publish static files
```
docker-compose run --rm web python /code/manage.py collectstatic
```

# Run app
```
docker-compose up
```
The API will be available at http://localhost:8080/posts/

# Tests

## Run unit tests
```
docker-compose --env-file .env.test run --rm web py.test posts/tests
```

## Run integration tests
Start services in test mode
```
docker-compose --env-file .env.test up
```

Run integration test
```
docker-compose exec web py.test posts/integration
```
