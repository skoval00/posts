from django.conf import settings
from django.db.models import F
from rest_framework import decorators, pagination, response, mixins, viewsets

from . import models, serializers


class PostPaginationClass(pagination.PageNumberPagination):
    page_size = settings.POSTS_PAGE_SIZE


class PostViewSet(mixins.CreateModelMixin,
                  mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    queryset = models.Post.objects.filter(promoted=False).order_by('-score')
    pagination_class = PostPaginationClass

    def get_serializer_class(self):
        if self.request.method.lower() == 'post':
            return serializers.CreatePostSerializer
        return serializers.PostSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(page, many=True)
        posts = serializer.data

        promoted_posts = list(models.Post.objects.filter(promoted=True).order_by('-score')[:2])
        if len(posts) >= settings.PROMOTED_PLACEMENT[0] + 2 and promoted_posts:
            position = settings.PROMOTED_PLACEMENT[0]
            if not (posts[position - 1]['nsfw'] or posts[position]['nsfw']):
                posts.insert(position, serializers.PostSerializer(promoted_posts.pop()).data)
        if len(posts) >= settings.PROMOTED_PLACEMENT[1] + 2 and promoted_posts:
            position = settings.PROMOTED_PLACEMENT[1]
            if not (posts[position - 1]['nsfw'] or posts[position]['nsfw']):
                posts.insert(position, serializers.PostSerializer(promoted_posts.pop()).data)

        return self.get_paginated_response(posts)

    @decorators.action(methods=['PATCH'], detail=True)
    def upvote(self, request, pk):
        queryset = self.get_queryset()
        result = queryset.filter(pk=pk).update(score=F('score') + 1)
        return response.Response({'upvoted': bool(result)})

    @decorators.action(methods=['PATCH'], detail=True)
    def downvote(self, request, pk):
        queryset = self.get_queryset()
        result = queryset.filter(pk=pk).update(score=F('score') - 1)
        return response.Response({'downvoted': bool(result)})
