import factory.django
import factory.fuzzy
import pytest

from posts import models


class PostFactory(factory.django.DjangoModelFactory):
    author = factory.fuzzy.FuzzyText()
    title = factory.fuzzy.FuzzyText()
    content = factory.fuzzy.FuzzyText()
    subreddit = factory.fuzzy.FuzzyText()
    score = factory.fuzzy.FuzzyInteger(-10, 10)
    promoted = False
    nsfw = False

    class Meta:
        model = models.Post


@pytest.fixture
def posts(db, request):
    PostFactory.create_batch(request.param)
    return request.param

