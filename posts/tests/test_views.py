import pytest
from django.conf import settings
from django.db.models import F
from django.urls import reverse

from posts import models
from posts import serializers
from .conftest import PostFactory


def test_create_post(db, client):
    response = client.post(
        reverse('post-list'),
        {
            'title': 'title',
            'author': 'author',
            'subreddit': 'subreddit',
            'content': 'content',
        },
    )
    assert response.status_code == 201
    data = response.json()
    post = models.Post.objects.get(pk=data['id'])
    assert data == serializers.CreatePostSerializer(post).data


@pytest.mark.parametrize("posts", [settings.POSTS_PAGE_SIZE + 1], indirect=["posts"])
def test_list_posts(client, posts):
    found_posts = []
    url = reverse('post-list')
    while url:
        response = client.get(url)
        assert response.status_code == 200
        data = response.json()
        posts_page = data['results']
        found_posts.extend([p["score"] for p in posts_page])
        url = data['next']
    assert len(found_posts) == posts
    assert found_posts == sorted(found_posts, reverse=True)


@pytest.mark.parametrize("posts", [settings.POSTS_PAGE_SIZE], indirect=["posts"])
def test_promoted_posts_order(client, posts):
    response = client.get(reverse('post-list'))
    assert response.status_code == 200
    posts = [p['id'] for p in response.json()['results']]
    models.Post.objects.filter(
        id__in=posts[:settings.PROMOTED_PLACEMENT[0] + 1]
    ).update(score=F('score') + 10)
    models.Post.objects.filter(
        id=posts[settings.PROMOTED_PLACEMENT[0]]
    ).update(nsfw=True)

    models.Post.objects.filter(
        id__in=posts[:settings.PROMOTED_PLACEMENT[1]]
    ).update(score=F('score') + 10)

    for _ in range(2):
        promoted_post = PostFactory.build(promoted=True)
        post_data = serializers.CreatePostSerializer(promoted_post).data
        del post_data['id']
        response = client.post(reverse('post-list'), post_data)
        assert response.status_code == 201

    response = client.get(reverse('post-list'))
    assert response.status_code == 200
    posts = [p['promoted'] for p in response.json()['results']]
    assert len(posts) == settings.POSTS_PAGE_SIZE + 1


@pytest.mark.parametrize('url_name, score', [
    ('post-upvote', 1),
    ('post-downvote', -1),
])
@pytest.mark.parametrize("posts", [1], indirect=["posts"])
def test_vote(client, posts, url_name, score):
    post = PostFactory(score=0)
    response = client.patch(reverse(url_name, args=[post.id]))
    assert response.status_code == 200
    post.refresh_from_db()
    assert post.score == score
