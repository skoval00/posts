from rest_framework import serializers

from . import models


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Post
        fields = '__all__'


class CreatePostSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Post
        exclude = ['score']

    def validate(self, attrs):
        link = attrs.get('link')
        content = attrs.get('content')
        if link and content:
            raise serializers.ValidationError('link and content fields are mutually exclusive')
        if not (link or content):
            raise serializers.ValidationError('either link or content field are mandatory')
        return attrs
