import json
from http import client


def test_api():
    conn = client.HTTPConnection('nginx')
    post_data = {
        'author': 'author', 'title': 'title', 'content': 'content', 'subreddit': 'subreddit',
    }
    conn.request(
        'POST', '/posts/',
        body=json.dumps(post_data),
        headers={'content-type': 'application/json'},
    )
    response = conn.getresponse()
    assert response.status == 201
    post_id = json.loads(response.read())['id']

    conn.request('PATCH', f'/posts/{post_id}/upvote/')
    response = conn.getresponse()
    assert response.status == 200
    assert json.loads(response.read()) == {'upvoted': True}

    conn.request('GET', f'/posts/')
    response = conn.getresponse()
    assert response.status == 200
    post = json.loads(response.read())['results'][0]
    assert post['id'] == post_id
    assert post['score'] == 1
