from django.apps import AppConfig


class RedditAppConfig(AppConfig):
    name = 'posts'
    label = 'posts'
