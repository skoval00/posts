from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

DEFAULT_VARCHAR_FIELD_LENGTH = 256


class Post(models.Model):
    title = models.CharField(max_length=DEFAULT_VARCHAR_FIELD_LENGTH)
    author = models.CharField(max_length=DEFAULT_VARCHAR_FIELD_LENGTH)
    subreddit = models.CharField(max_length=DEFAULT_VARCHAR_FIELD_LENGTH)
    link = models.URLField(max_length=DEFAULT_VARCHAR_FIELD_LENGTH, blank=True)
    content = models.TextField(blank=True)
    score = models.IntegerField(default=0)
    promoted = models.BooleanField(default=False)
    nsfw = models.BooleanField(default=False)

    def clean(self):
        if self.link and self.content:
            raise ValidationError(_('link and content fields are mutually exclusive'))
        if not (self.link or self.content):
            raise ValidationError(_('either link or content field are mandatory'))
