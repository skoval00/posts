from django.urls import include, path
from rest_framework import routers

from . import views

router = routers.SimpleRouter()
router.register(r'posts', views.PostViewSet)

urlpatterns = [
    path('drf/', include('rest_framework.urls')),
] + router.urls
